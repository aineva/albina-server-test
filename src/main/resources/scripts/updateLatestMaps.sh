rm $1/latest/*.jpg
rm $1/latest/*.webp
rm $1/latest/*.png

cp $1/$2/*.jpg $1/latest/
cp $1/$2/*.webp $1/latest/
cp $1/$2/*.png $1/latest/

for region in EUREGIO IT-21 IT-23 IT-25 IT-32-BZ IT-32-TN IT-34 IT-36 IT-57; do  # 242ad570-0cf5-4c55-944b-9d07bb03c553
  rm $1/latest/${region}_*.pdf
  cp $1/$2/${region}_*.pdf $1/latest/
  for daytime in fd am pm; do
    rm $1/latest/${daytime}_${region}_*.pdf
    cp $1/$2/${daytime}_${region}_*.pdf $1/latest/
  done
done

chmod -R 755 $1/latest/

