/*******************************************************************************
 * Copyright (C) 2019 Norbert Lanzanasto
 * Contribution by DkR S.r.l. for AINEVA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package eu.albina.rest;

import com.github.openjson.JSONArray;
import com.github.openjson.JSONObject;
import com.google.common.base.Strings;
import eu.albina.controller.AvalancheReportController;
import eu.albina.controller.RegionController;
import eu.albina.controller.ServerInstanceController;
import eu.albina.controller.UserController;
import eu.albina.exception.AlbinaException;
import eu.albina.map.MapUtil;
import eu.albina.model.AvalancheBulletin;
import eu.albina.model.AvalancheReport;
import eu.albina.model.Region;
import eu.albina.model.ServerInstance;
import eu.albina.model.enumerations.BulletinStatus;
import eu.albina.model.enumerations.LanguageCode;
import eu.albina.model.enumerations.Role;
import eu.albina.rest.filter.Secured;
import eu.albina.util.AlbinaUtil;
import eu.albina.util.PdfUtil;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.Parameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Path("/bulletins_preview_resources")
@Tag(name = "bulletins_preview_resources")
@OpenAPIDefinition(info = @Info(
	title = "albina-server",
	version = "0.0",
	description = "Server component to compose multilingual avalanche bulletins resources before publication, for regional services internal use",
	license = @License(name = "GNU General Public License v3.0", url = "https://gitlab.com/albina-euregio/albina-server/-/blob/master/LICENSE"),
	contact = @Contact(name = "AINEVA", url = "https://aineva.it/", email = "aineva@aineva.it")
), servers = {@Server(url = "/albina/api")})
public class AvalancheBulletinPrevResService {

	private static final Logger logger = LoggerFactory.getLogger(AvalancheBulletinPrevResService.class);

	@Context
	UriInfo uri;

	@POST
	@Secured({Role.ADMIN, Role.FORECASTER, Role.FOREMAN, Role.OBSERVER})
	@SecurityRequirement(name = AuthenticationService.SECURITY_SCHEME)
	@Path("/generate")
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Generate bulletins resources for date")
	public Response getPreviewResources(
		@Parameter(description = DateControllerUtil.DATE_FORMAT_DESCRIPTION) @QueryParam("date") String date
		, @QueryParam("region") String regionId
		, @QueryParam("lang") LanguageCode language
		, @Context SecurityContext securityContext
	) {
		/*
			NOTE: the source code of this method is a modified copy of eu.albina.rest.AvalancheBulletinService.getPreviewPdf

            This method assumes that:

                - The directory /bulletins_preview_resources at the root of the filesystem is writable by this software.
                  You may have to check /etc/systemd/system/multi-user.target.wants/tomcat9.service

                        [Service]
                        #Security
                        ReadWritePaths=/bulletins_preview_resources/

                  Then systemctl daemon-reload; systemctl restart tomcat9

                - The directory /bulletins_preview_resources is publicly accessible via some file server.
                  You may add the following to nginx.conf or /etc/nginx/sites-available/albina

                        location ~ ^/bulletins_preview_resources(/.+)$ {
                                root /bulletins_preview_resources;
                                try_files $1 $1/ =404;
                                add_header Cache_Control "max-age=86400, public";
                                autoindex on;
                                autoindex_exact_size off;
                                autoindex_format json;
                                autoindex_localtime off;
                        }

                  Then nginx -t; nginx -s reload
         */

		String requestId;
		{
			String requestTimeString = AlbinaUtil.getZonedDateTimeNowNoNanos().format(
				DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmm").withZone(ZoneId.of("UTC")));
			String randomString = new BigInteger(128, new SecureRandom()).toString(36);
			requestId = requestTimeString + "Z" + randomString;
		}

		logger.debug("POST generate preview resources [{}, {}, {}, {}]", date, regionId,
			securityContext.getUserPrincipal().getName(), requestId);

		try {
			if (Strings.isNullOrEmpty(date)) {
				return Response.status(400).type(MediaType.APPLICATION_JSON).entity("{\"error\":\"missing date\"}").build();
			}
			if (Strings.isNullOrEmpty(regionId)) {
				return Response.status(400).type(MediaType.APPLICATION_JSON).entity("{\"error\":\"missing region\"}").build();
			}
			if (language == null) {
				return Response.status(400).type(MediaType.APPLICATION_JSON).entity("{\"error\":\"missing lang\"}").build();
			}
			Instant startDate = ZonedDateTime.parse(date).toInstant();
			Region region = RegionController.getInstance().getRegionOrThrowAlbinaException(regionId);

			AvalancheReport report = AvalancheReportController.getInstance().getInternalReport(startDate, region);
			if (report == null) {
				return Response.noContent().build();
			}
			String bulletinsJsonArrayString = report.getJsonString();
			if (bulletinsJsonArrayString == null) {
				// TODO?: is this condition possible?
				return Response.noContent().build();
			}

			// NOTE: copying older code: that report json string is an array of bulletins

			JSONArray bulletinsJsonArray = new JSONArray(bulletinsJsonArrayString);
			List<AvalancheBulletin> bulletins = new ArrayList<>();
			for (Object object : bulletinsJsonArray) {
				if (object instanceof JSONObject) {
					AvalancheBulletin bulletin = new AvalancheBulletin((JSONObject) object, UserController.getInstance()::getUser);
					if (bulletin.affectsRegionWithoutSuggestions(region)) {
						bulletins.add(bulletin);
					}
				}
			}
			Collections.sort(bulletins);

			java.nio.file.Path baseDirectory = Paths.get("/bulletins_preview_resources", requestId);
			java.nio.file.Path outputDirectory = baseDirectory.resolve("output");

			ServerInstance serverInstance = ServerInstanceController.getInstance().getLocalServerInstance();
			serverInstance.setMapsPath(outputDirectory.toString());
			serverInstance.setPdfDirectory(outputDirectory.toString());

			AvalancheReport avalancheReport = AvalancheReport.of(bulletins, region, serverInstance);

			/*
				NOTE: The logic that determines which *bulletins* are included in a *report* is currently
					  embedded within the *PDF report* generation process.

					  More specifically, this logic resides in the PdfUtil class, which handles PDF rendering.
					  It is not found in the AvalancheReport class,
					  nor in the procedure that creates the List<AvalancheBulletin> bulletins.

					  The PDF report consists of map images, with one image generated for each bulletin.
					  PdfUtil is responsible for generating the PDF,
					  while the map images must be prepared in advance using the MapUtil class.
					  This results in the report composition logic being duplicated,
					  with the same logic also present in MapUtil.

					  This duplication introduces the risk of inconsistencies in the report composition logic.

					  Additionally, the original software does not support generating PDF reports that:
						- Include saved but unpublished bulletins (draft) — report composition
						- Render the PDF report as if it were already published (not draft) — report rendering.

					  To avoid rewriting three critical system components, a workaround is needed.
					  Specifically, we must define the report state before using MapUtil
					  and adjust it afterward before using PdfUtil.
			*/

			avalancheReport.setStatus(BulletinStatus.draft); // NOTE: createMapyrusMaps considers published bulletins in all cases + saved only if draft or updated
			MapUtil.createMapyrusMaps(avalancheReport);

			avalancheReport.setStatus(BulletinStatus.submitted); // NOTE: to avoid "preview" in the title
			PdfUtil pdfBuilder = new PdfUtil(avalancheReport, language, false);
			pdfBuilder.createPdf();

			// Move the generated PDF to a known location.
			Files.move(pdfBuilder.getPath(), baseDirectory.resolve("report.pdf"));

			// Let's avoid that structure of outputDirectory and move all under baseDirectory
			try (Stream<java.nio.file.Path> stream = Files.walk(baseDirectory)) {
				for (java.nio.file.Path path : stream.collect(Collectors.toList())) {
					if (Files.isRegularFile(path)) {
						Files.move(path, baseDirectory.resolve(path.getFileName()));
					}
				}
			}
			// So remove the outputDirectory
			// Files.delete(outputDirectory); // This would fail if we fail to remove the nested directories...

			// Prepare output json object
			JSONObject resourcesJson = new JSONObject();
			resourcesJson.put("requestId", requestId);
			resourcesJson.put("requestedBy", securityContext.getUserPrincipal().getName());
			resourcesJson.put("reportId", report.getId());
			resourcesJson.put("reportDate", DateTimeFormatter.ISO_INSTANT.format(report.getDate()));
			resourcesJson.put("reportStatus", report.getStatus().toString());
			resourcesJson.put("reportRegion", report.getRegion());
			resourcesJson.put("validityDateString", AlbinaUtil.getValidityDateString(bulletins));
			// resourcesJson.put("publicationTimeString", publicationTimeString);
			resourcesJson.put("bulletins", bulletinsJsonArray);

			try (PrintWriter out = new PrintWriter(baseDirectory.resolve("resources.json").toFile())) {
				out.print(resourcesJson);
			}

			try (Stream<java.nio.file.Path> stream = Files.walk(baseDirectory)) {
				Set<PosixFilePermission> f_perms = new HashSet<>();
				f_perms.add(PosixFilePermission.OWNER_READ);
				f_perms.add(PosixFilePermission.GROUP_READ);
				f_perms.add(PosixFilePermission.OTHERS_READ);

				Set<PosixFilePermission> d_perms = new HashSet<>();

				d_perms.add(PosixFilePermission.OWNER_READ);
				d_perms.add(PosixFilePermission.GROUP_READ);
				d_perms.add(PosixFilePermission.OTHERS_READ);
				d_perms.add(PosixFilePermission.OWNER_EXECUTE);
				d_perms.add(PosixFilePermission.GROUP_EXECUTE);
				d_perms.add(PosixFilePermission.OTHERS_EXECUTE);

				for (java.nio.file.Path path : stream.collect(Collectors.toList())) {
					if (Files.isRegularFile(path))
						Files.setPosixFilePermissions(path, f_perms);
					if (Files.isDirectory(path))
						Files.setPosixFilePermissions(path, d_perms);
				}
			}

			resourcesJson.remove("bulletins");
			logger.debug("Successfully completed generation of preview resources [{}, {}, {}, {}]", date, region,
				securityContext.getUserPrincipal().getName(), requestId);
			return Response.ok(resourcesJson.toString(), MediaType.APPLICATION_JSON).build();
		} catch (AlbinaException e) {
			logger.warn("Error creating preview resources [" + requestId + "]", e);
			JSONObject err = e.toJSON();
			err.put("requestId", requestId);
			return Response.status(500).type(MediaType.APPLICATION_JSON).entity(err.toString()).build();
		} catch (Exception e) {
			logger.warn("Error creating preview resources [" + requestId + "]", e);
			JSONObject err = new JSONObject();
			err.put("requestId", requestId);
			err.put("message", "server error");
			return Response.status(500).type(MediaType.APPLICATION_JSON).entity(err.toString()).build();
		}
	}
}
