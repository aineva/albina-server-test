package eu.albina.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tool that retrieves the maximum DPI of images contained in a PDF file.
 * Depends on `xpdf-utils` debian package.
 */
public class MapDpiGetter {
	//                                               page         num        type       width       height      color        comp          bpc         enc         interp      object          ID 		x-ppi 	     y-ppi 				size ratio
	static Pattern pattern = Pattern.compile("^[\\s]*[\\d]+[\\s]+[\\d]+[\\s]+image[\\s]+[\\d]+[\\s]+[\\d]+[\\s]+[^\\s]+[\\s]+[\\d]+[\\s]+[\\d]+[\\s]+image[\\s]+[^\\s]+[\\s]+[^\\s]+[\\s]+[^\\s]+[\\s]+(\\d+)[\\s]+(\\d+)[\\s]+.*");

	static int getMaxDpi(Path pdfFile) throws IOException {
		ProcessBuilder pb = new ProcessBuilder("pdfimages", "-list", pdfFile.toString());
		Process process = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line;
		int maxPpi = 10;

		while ((line = reader.readLine()) != null) {
			Matcher matcher = pattern.matcher(line);
			if (matcher.find()) {
				int xppi = Integer.parseInt(matcher.group(1));
				int yppi = Integer.parseInt(matcher.group(2));
				if (xppi > maxPpi) {
					maxPpi = xppi;
				}
				if (yppi > maxPpi) {
					maxPpi = yppi;
				}
			}
		}

		process.destroy();
		reader.close();
		return maxPpi;
	}
}
